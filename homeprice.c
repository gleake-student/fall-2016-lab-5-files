#include <stdio.h> 
#include <stdlib.h>

int main(){
    
    FILE *fp;//opens file to file pointer
    fp=fopen("homelistings.csv", "r");//opens file and reads it

    if (!fp){//checks to see if file pointer (fp) has anything in it or has problems
        printf("Couldn't open homelistings.csv\n");
        exit(1);//exits program if nothing is there or there is a problem
    }
    
    long priceTotal=0;//creates total variable 
    int zipcode, houseID, housePrice, bedrooms, baths, squareFoot, lowPrice, highPrice, avg, count = 0;
    //variables to use in working in the contents of the file
    char address[50];
    
    while (fscanf(fp, " %d,%d,%[^,],%d,%d,%d,%d", &zipcode, &houseID, address, &housePrice, &bedrooms, &baths, &squareFoot) != EOF) {
        //scans through looking for numbers and the end of an area by looking for a comma
        //scans and runs until end of file
        if (count == 0) {
            lowPrice = housePrice;
            highPrice = housePrice;
        } else {
            if (housePrice < lowPrice) {
                lowPrice = housePrice;
            } else if (highPrice > lowPrice) {
                highPrice = housePrice;
            }
        }
        priceTotal += housePrice;
        count++;adds 1 to the count of the number of houses
    }
    avg = priceTotal / count;//averages the total price by the number of houses
    printf("%d %d %d\n", lowPrice, highPrice, avg);//
    fclose(fp);
    
    
    
}