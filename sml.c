#include <stdio.h> 
#include <stdlib.h>

int main(){
    
    
    FILE *startFile, *small, *medium, *large;
    startFile = fopen("homelistings.csv", "r");//read from homelistings.csv
    small = fopen("smallhomes.txt", "w");//use fopen and "w" to open and write files
    medium = fopen("mediumhomes.txt", "w");
    large = fopen("largehomes.txt", "w");
    
    if (!startFile){
        printf("Couldn't open startFile\n");
        exit(1);
    }
    
    int zipcode, houseID, housePrice, bedrooms, baths, squareFoot;
    char address[50];
    
     
    while (fscanf(startFile, " %d,%d,%[^,],%d,%d,%d,%d", &zipcode, &houseID, address, &housePrice, &bedrooms, &baths, &squareFoot) != EOF) {
        //scans through file for the numbers and files until it hits end of file. runs loop to go through entire file
        if (squareFoot < 1000) {// if the house is less than 1000 square feet it categorizes it as a small home and prints it to that file
            fprintf(small, "%s : %d\n", address, squareFoot); 
        } 
        else if (squareFoot >= 1000 && squareFoot <= 2000) {
            // if the house is more than 1000 but less than 2000 square feet it categorizes it as a medium home and prints it to that file
            fprintf(medium, "%s : %d\n", address, squareFoot);    
        }
        else {// if the house is more than 2000 square feet it categorizes it as a large home and prints it to that file
            fprintf(large, "%s : %d\n", address, squareFoot);
        }
        
    }
    
    fclose(startFile);
}