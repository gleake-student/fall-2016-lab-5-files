#include <stdio.h> 
#include <stdlib.h>

int main(){
    
    
    FILE *startFile;
    startFile = fopen("homelistings.csv", "r");//opens and reads file to get the housing information
    
    
    if (!startFile){
        printf("Couldn't open startFile\n");
        exit(1);
    }
    
    int zipcode, houseID, housePrice, bedrooms, baths, squareFoot;
    char address[50];
    
     
    while (fscanf(startFile, " %d,%d,%[^,],%d,%d,%d,%d", &zipcode, &houseID, address, &housePrice, &bedrooms, &baths, &squareFoot) != EOF) {
        char filename[10];//allows for the name of the file
        sprintf(filename, "%d.text", zipcode);//names the file and creates a .txt file that goes by zip code
        FILE *zipCodes = fopen(filename, "a");//opens and appends the files
        if (!zipCodes) {
            printf("Can't open file: %s\n", filename);
        }
        fprintf(zipCodes, "%s\n", address);//prints the addresses to the appropriate file based on zip code
        fclose(zipCodes);//closes file when done
        
    }
    
    fclose(startFile);//closes main file
}